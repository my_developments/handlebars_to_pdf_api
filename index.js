const cors = require('cors');
const express = require('express');
const hbsToPdf = require('./routes/hbsToPdf');

const app = express();

/* ==================================================================================================
=========================================== Middleware ==============================================
===================================================================================================*/

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', hbsToPdf);

// Configure headers and cors
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

/* ==================================================================================================
============================================== Routes ===============================================
===================================================================================================*/

app.get('/', (req, res) => {
  return res.status(200).send("by Vainilla Dev");
});

const port = process.env.port || 3001;
app.listen(port, () => {
  console.log("Listening on port: " + port);
});