const express = require('express');
const fs = require('fs');
const pdf = require('handlebars-pdf');

const router = express.Router();

var ex = fs.readFileSync('./temp/temp.hbs', 'utf-8');
let document = {
  template: ex,
  context: {
      msg: 'Hello world'
  },
  path: "./temp/temp.pdf"
}

router.get('/hbsToPdf', async(req, res) => {
  await pdf.create(document)
    .then(response => {
      console.info("File converted successfully | ", response);
      res.status(200).json({
        "status": "200",
        "message": "OK",
        "payload": {
            "response": response,
        }
      });
    })
    .catch(error => {
      console.error("Fails throughout file conversion | ", error);
      res.status(400).json({
        "status": "400",
        "message": "Fail",
        "payload": {
          "error": error
        }
      });
    })
});

module.exports = router;